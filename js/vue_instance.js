var  vm = new Vue({
   el: '#vue_det', // element id
   data: {
      firstname : "Ria",
      lastname  : "Singh",
      address    : "Mumbai"
   },
   methods: {
      mydetails : function() {
         return "I am "+this.firstname +" "+ this.lastname;
      }
   }
});

var _obj = { fname: "Raj", lname: "Singh"};
         
// direct instance creation
var vm = new Vue({
   data: _obj
});
console.log(vm.fname);
console.log(vm.$data);
console.log(vm.$data.fname);

// must use function when in Vue.extend()
var Component = Vue.extend({
   data: function () {
      return _obj
   }
});
var myComponentInstance = new Component();
console.log(myComponentInstance.lname);
console.log(myComponentInstance.$data);